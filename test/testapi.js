//Require para importar librerias de testing
var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var miApp = require('../server');
var should = chai.should()

//Inicializar chai con chai-http
chai.use(chaiHttp);

describe('Test de Conectividad', () => {
 it('Google funciona', (done) => {
   chai.request('https://www.google.es')
       .get('/')
       .end((err, res) => {
            console.log("Status recibido: "+res.status);
            res.should.have.status(200);
            done()
       });
 })
})

describe('Tests de API usuarios', () => {
  it('Responde', (done) => {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1')
        .end((err, res) => {
             console.log("Status recibido: "+res.status);
             res.should.have.status(200);
             res.body.bienvenida.should.be.eql("Bienvenido a API de Javier");
             done()
        });
  });

  it('Devuelve lista de usuarios', (done) => {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1/usuarios')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array')
          for (user of res.body) {
            user.should.have.property('email');
            user.should.have.property('password');
          }
          done()
        })
  });


})
