var express = require('express');  //IMPORTAMOS LA LIBRERIA EXPRESS
var bodyParser = require('body-parser'); //importamos body-parser (LIBRERIA para procesar el body de la request)
var app = express(); //INICIALIZA UNA INSTACIA DE EXPRESS
app.use(bodyParser.json()); //inicializa el body parser dentro de nuestra app para que pueda parsear json dentro del body
var port = process.env.PORT || 3000; //BUSCA EN VARIABLES DE ENTORNO O EL 3000 POR DEFECTO
var usersFile = require('./login_users.json');
var requestJson = require('request-json');
var cors = require('cors');
var dateFormat = require('dateformat');
var fs = require('fs');
//inicializa servidor express.js
console.log("Inicializando API Tech U JGR/BMR");
app.listen(port);
console.log("API escuchando en el puerto: "+port+ ", son las: "+dateFormat);
console.log("Express server inicializado.");


//URL conexion MongoDB (Mlab)
var baseURLMlab = 'https://api.mlab.com/api/1/databases/techujgr/collections/';
var apiKey = 'apiKey=7GVdM0OdEO29vIahZPu-c0THCqvKoj4o';
var clienteMlab = null;

app.get('/apitechu/v1', function(request, response){
    response.send({"bienvenida":"Bienvenido a API de Javier", "Hoy es":new Date()});
})

app.get('/apitechu/v1/usuarios', function(req, res){
  res.send(usersFile);
})

app.post('/apitechu/v1/usuarios', function(req, res){
  var nuevo = {"first_name":req.headers.first_name, "country":req.headers.country, "last_name":req.headers.last_name, "id":req.headers.id};
  usersFile.push(nuevo);
  console.log(req.headers);
  const datos = JSON.stringify(usersFile);
  fs.writeFile("./usuarios.json", datos, function(err){
    if (err)
      console.log(err);
    console.log("Fichero guardado");
  });

  res.send("Alta OK");
})

app.post('/apitechu/v2/usuarios', function(req, res){

  var nuevo = req.body;
  usersFile.push(nuevo);
  console.log(req.body);
  const datos = JSON.stringify(usersFile);
  fs.writeFile("./usuarios.json", datos, function(err){
    if (err)
      console.log(err);
    console.log("Fichero guardado");
  });

  res.send("Alta OK v2");
})
app.delete('/apitechu/v1/usuarios/:id', function(req, res){
  usersFile.splice(req.params.id-1, 1);
  res.send("Usuario borrado");
})

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req, res){
  console.log("parametros");
  console.log(req.params);
  console.log("Query-String");
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers);
  console.log("Body");
  console.log(req.body)
  res.send("respuesta")
})

app.post('/apitechu/v1/login', function(req, res){
  var attemptingUser = {"email":req.headers.email, "password":req.headers.password};
  var userExists = 'N';
  var i = 0;
  while (i < usersFile.length && userExists == 'N') {
    var currentUser = usersFile[i];
    if (currentUser.email == attemptingUser.email && currentUser.password == attemptingUser.password){
      //el usuario existe y la contraseña coincide
      currentUser.isLogged = 'Y';
      userExists = 'Y';
      usersFile[i] = currentUser;
      attemptingUser = currentUser;
    }
    i++;
  }

  if(userExists == 'N'){
    res.send({"encontrado":"NO"});
  }else if (userExists == 'Y') {
    //Usuario logado correctamente
    //actualizamos el fichero de usuarios
    const datos = JSON.stringify(usersFile);
    fs.writeFile("./login_users.json", datos, function(err){
      if (err)
        console.log(err);
      console.log("Fichero de login guardado");
    });
    console.log('USUARIO: '+attemptingUser.email+' LOGADO');
    res.send({"encontrado":"SI", "id":attemptingUser.id});
  }
})

app.post('/apitechu/v1/logout', function(req, res){
  var attemptingUser = {"id":req.headers.id};
  console.log("intentando login de usuario "+req.headers.id);
  var userExists = 'N';
  var i = 0;
  while (i < usersFile.length && userExists == 'N') {
    var currentUser = usersFile[i];
    if (currentUser.id == attemptingUser.id && currentUser.isLogged == 'Y'){
      //el usuario existe, la contraseña coincide, y tiene sesion abierta
      currentUser.isLogged = 'N';
      userExists = 'Y';
      usersFile[i] = currentUser;
      attemptingUser = currentUser;
    }
    i++;
  }

  if(userExists == 'N'){
    res.send({"encontrado":"NO"});
  }else if (userExists == 'Y') {
    //Usuario logado correctamente
    //actualizamos el fichero de usuarios
    const datos = JSON.stringify(usersFile);
    fs.writeFile("./login_users.json", datos, function(err){
      if (err)
        console.log(err);
      console.log("Fichero de login guardado");
    });
    console.log('USUARIO: '+attemptingUser.email+' HA FINALIZADO SESION');
    res.send({"encontrado":"SI", "id":attemptingUser.id});
  }
})


//*******************************************Bloque de Cuentas************************//
var cuentas = require('./cuentas.json');
app.get('/apitechu/v5/cargarCuentasUsuario', function(req, res){
  //console.log(req.headers);
  var userId = req.headers.id;
  var query = 'q={"idCliente": '+userId+'}';
  var filtro = '&f={"iban":1, "saldo":1, "_id":0}';
  console.log(query);
  clienteMlab = requestJson.createClient(baseURLMlab + "/cuentas?"+query+filtro+'&'+apiKey);
  console.log('Buscando cuentas de usuario: '+userId);
  clienteMlab.get('', function(err, resM, body){
    if (!err){
      res.send(body);
    }
  })

/*
  clienteMlab.get('', function(err, resM, body){
    var results =[];
    for (var i = 0; i < body.length; i++) {
      console.log();
      results[i] = body[i].iban;
    }
    console.log('Se han encontrado: '+results.length+" cuentas, para el usuario: "+userId);
    res.send(results);
  });*/

});

app.get('/apitechu/v5/cargarcuentasglobal', function(req, res){
  //console.log(req.headers);
  console.log('Buscando cuentas de usuario');
  var results = [];
  for (var i = 0; i < cuentas.length; i++) {
      results.push(cuentas[i].iban);
  }
  res.send(results);

});

app.get('/apitechu/v5/cargarmovimientos', function(req, res){
  var iban = req.headers.iban;
  var query = 'q={"iban": "'+iban+'" }&f={"movimientos":1, "_id":0}';
  //console.log(query);
  clienteMlab = requestJson.createClient(baseURLMlab + "/cuentas?"+query+'&'+apiKey);
  //console.log('Buscando movimientos de iban: '+iban);
  clienteMlab.get('', function (err, resM, body){
    if(!err){
      body[0].movimientos = body[0].movimientos.sort(function(a,b){
        return ((a.fecha == b.fecha) ? 0 : ((a.fecha > b.fecha) ? -1 : 1 ));
      })
      res.send(body[0]);
    }

  });
});

app.post('/apitechu/v5/transfer', function(req, res){
  console.log(req.headers);
  var cuentaOrigen = req.headers.cuentaorigen;
  var saldoOrigen = req.headers.saldoorigen;
  var importe = req.headers.importe;
  var concepto = req.headers.concepto;
  var cuentaDestino = req.headers.cuentadestino;

  //leer saldo CuentaDestino
  var saldoDestino = Number(0);
  var query = 'q={"iban":"'+cuentaDestino+'"}&f={"saldo":1, "_id":0}&fo=true';
  clienteMlab = requestJson.createClient(baseURLMlab + "/cuentas?"+query+'&'+apiKey);
  clienteMlab.get('', function (err, resM, body){
    // console.log("consultamos saldo destino");
    if(!err){
      //Sumamos saldo destino
      // console.log("Saldo Destino Antes: "+body.saldo);
      saldoDestino = Number(body.saldo) + Number(importe);
      // console.log("Saldo Destino Despues: "+saldoDestino);

      //contamos cuantos movimientos tiene en cuenta Destino
      // console.log("movimientos en cuenta destino: "+cuentaDestino);
      query = 'q={"iban": "'+cuentaDestino+'" }&f={"movimientos":1, "_id":0}';
      var nMovs = 0;
      movimientos = [];
      clienteMlab = requestJson.createClient(baseURLMlab + "/cuentas?"+query+'&'+apiKey);
      var result = clienteMlab.get('', function (err2, resM2, body2){
        if(!err2){
        // console.log("contamos cuantos movimientos tiene en cuenta Destino");
        // console.log(body2[0].movimientos);
        nMovs = +body2[0].movimientos.length;
        movimientos = body2[0].movimientos;
        // console.log("Movimientos antes de insertar el nuevo: "+movimientos);
        //insertamos movimiento positivo en cuenta Destino
        var nuevo = {"id":nMovs+1, "fecha":dateFormat(new Date(),"yyyy/mm/dd"), "importe":importe, "concepto":concepto};
        movimientos.push(nuevo);
        // console.log("Movimientos despues de insertar el nuevo: "+ movimientos);
        clienteMlab.put('', {"$set":{"movimientos":movimientos}}, function(errUp, resUp, bodyUp){
          // console.log("insertamos movimiento positivo en cuenta Destino");
          // console.log(bodyUp);
          // console.log("se ha actualizado el listado de movimientos en destino: "+result);

          //actualizamos saldo en cuenta Destino
          clienteMlab.put('', {"$set":{"saldo":saldoDestino}}, function(errSD, resSD, bodySD){
            // console.log("actualizamos saldo en cuenta Destino");
            // console.log(bodySD);

            //descontamos saldo en cuenta cuentaOrigen
            query = 'q={"iban":"'+cuentaOrigen+'"}&f={"_id":0}&fo=true';
            // console.log("Saldo Origen antes: "+saldoOrigen);
            saldoOrigen = Number(saldoOrigen)-Number(importe);
            // console.log("Saldo Origen descontado: "+saldoOrigen);
            clienteMlab = requestJson.createClient(baseURLMlab + "/cuentas?"+ query+'&'+apiKey);
            clienteMlab.put('', {"$set":{"saldo":saldoOrigen}}, function(errSO, resSO, bodySO){
              // console.log("descontamos saldo en cuenta cuentaOrigen");
              // console.log(bodySO);

              // //contamos numero de movimientos en origen
              clienteMlab.get('', function (errSO2, resSO2, bodySO2){
                if(!err2){
                // console.log("contamos numero de movimientos en origen");
                // console.log(bodySO2);
                movimientos = bodySO2.movimientos;
                nMovs = bodySO2.movimientos.length;
                // console.log("movis: "+ movimientos);
                // console.log("n: "+ nMovs);
                //insertamos movimiento negativo en cuentaOrigen
                nuevo = {"id":nMovs+1, "fecha":dateFormat(new Date(),"yyyy/mm/dd"), "importe":-importe, "concepto":concepto};
                movimientos.push(nuevo);
                clienteMlab.put('', {"$set":{"movimientos":movimientos}}, function(errSO3, resSO3, bodySO3){
                  // console.log("insertamos movimiento negativo en cuentaOrigen");
                  // console.log(bodySO3);
                  res.status(200).send({"cuenta Actualizada": cuentaDestino ,"Transferencia": importe});
                });
                }
              });

            });
          });
        });

        }
      });
    } else {
      console.log("Error en la Transferencia");
      res.status(500).send({"Error":"Error en la transferencia"});
    }
  });



});


app.get('/apitechu/v5/usuarios', function (req, res) {
  var query = 'f={"first_name": 1, "last_name": 1, "_id":0}';
  clienteMlab = requestJson.createClient(baseURLMlab + "/usuarios?"+query+'&'+apiKey);
	clienteMlab.get('', function(err, resM, body){
    if (!err){
      res.send(body);
    }
  })
});

app.get('/apitechu/v5/usersLogin', function (req, res) {
  //var query = 'f={"first_name": 1, "last_name": 1, "_id":0}';
  clienteMlab = requestJson.createClient(baseURLMlab + "/usuarios?"+apiKey);
	clienteMlab.get('', function(err, resM, body){
    if (!err){
      res.send(body);
    }
  })
});

//Saca Nombre y Apellido de un usuario a partir de su id
app.get('/apitechu/v5/getInfoUsuario/:id', function(req, res){
  var id = req.params.id;
  //var query = 'q={ "id": '+id+' }';
  console.log("Cargando info de usuario: "+id);
  var query = 'f={"first_name": 1, "last_name": 1}&q={ "id": '+id+' }';
  clienteMlab = requestJson.createClient(baseURLMlab + "/usuarios?"+query+'&l=1&'+apiKey);

	clienteMlab.get('', function(err, resM, body){
    if (!err && body.length > 0){
      res.send({"nombre":body[0].first_name, "apellido": body[0].last_name});
      //res.send(body[0]);
    }else{
      res.status(404).send('Usuario NO encontrado');
    }
  })
})

app.post('/apitechu/v5/usuarios/login', function(req, res){
  var email=req.headers.email;
  var password=req.headers.password;
  console.log('Intento de Login de usuario: '+email);
  var query = 'q={ $and:[{"email": "'+email+'"}, {"password":"'+password+'"}]}'
  console.log(query);
  clienteMlab = requestJson.createClient(baseURLMlab + "/usuarios?"+query+'&l=1&'+apiKey);
  clienteMlab.get('', function(err, resM, body){

    if (!err){
      if (body.length == 1){
        console.log('Login Correcto');
        clienteMlab = requestJson.createClient(baseURLMlab + "/usuarios?"+query+'&'+apiKey);
        //var cambio = '{"$set":{"isLogged":true}}';
        //clienteMlab.put('', JSON.parse(cambio), function(errUp, resUp, bodyUp){
        clienteMlab.put('', {"$set":{"isLogged":true}}, function(errUp, resUp, bodyUp){
          console.log(bodyUp);
          res.status(200).send({"usuario": body[0].id ,"mensaje": "login correcto", "registros_actualizados": bodyUp.n});
        });


      }else{
        res.status(404).send('Usuario NO encontrado');
      }

    }else{
      console.log('Error en la query');
      res.status(400).send('Error en acceso a bbdd');
    }
  });
});

app.post('/apitechu/v5/usuarios/logout', function(req, res){
  var id=req.headers.id;
  console.log('Intento de Login de usuario: '+id);
  var userExists = false;
  var query = 'q={ "id": '+id+', "isLogged": true}';
  console.log(query);
  clienteMlab = requestJson.createClient(baseURLMlab + "/usuarios?"+query+'&l=1&'+apiKey);
  clienteMlab.get('', function(err, resM, body){
    if (!err){
      if (body.length == 1){//Estaba logado
        clienteMlab = requestJson.createClient(baseURLMlab + "/usuarios?"+query+'&'+apiKey);
        clienteMlab.put('', {"$set":{"isLogged":false}}, function(errP, resP, bodyP){
          res.send({"usuario":body[0].id, "mensaje":"logout correcto","registros_actualizados": bodyP.n});
        });
      }else{
        res.send('Usuario NO logado previamente');
      }

    }else{
      console.log('Error en la query');
      res.status(400).send('Error en acceso a bbdd');
    }
  });
});

app.post('/apitechu/v5/registro', function(req, res) //http://localhost:3000/apitechu/v5/registro
{
  var first_name = req.headers.first_name;
  var last_name = req.headers.last_name;
  var email = req.headers.email;
  var password = req.headers.password;
  var gender = req.headers.gender;
  var country = req.headers.country;

  var query = 'q={"email":"' + email + '"}&f={"id":1, "first_name":1, "last_name":1, "_id":0}';
  clienteMlab = requestJson.createClient(baseURLMlab + "/usuarios?" + query + "&l=1&" + apiKey);
  clienteMlab.get('', function(err, resM, body)
  {
    if(!err)
    {
      if(body.length == 1)
      {
        res.status(404).send("Ya existe un usuario con ese email");
      }
      else
      {
        console.log("Se puede realizar el registro");
        var query = 'q={"password":{$exists: true }}&f={"id":1}&s={"id":-1}&l=1';
        clienteMlab = requestJson.createClient(baseURLMlab + "/usuarios?" + query + "&" + apiKey);
        clienteMlab.get('', function(errX, resX, bodyX)
        {
          if(!errX)
          {
            if(bodyX.length == 1)
            {
              var id = bodyX[0].id +1;
              console.log(id);
              clienteMlab = requestJson.createClient(baseURLMlab + "/usuarios");
              var nuevo = '{"first_name": "' + first_name + '","last_name": "' + last_name
              + '" , "email" :"' + email + '", "password": "' + password +
              '", "id":' + id +', "gender":"' + gender + '", "country":"' + country + '"}';
              clienteMlab.post('?' + apiKey, JSON.parse(nuevo), function(errP, resP, bodyP)
              {
                if(!errP)
                {
                  res.send({"nuevo":"ok", "first_name":first_name, "last_name":last_name,
                  "email": email, "password": password, "id": id, "gender": gender,
                  "country": country});
                }
                else res.status(404).send("Error en registro");
              });
            }
            else res.status(404).send("Error en registro");
          }
          else res.status(404).send("Error en registro");
        });
      }
    }
    else res.status(404).send("Error en registro");
  });
});
